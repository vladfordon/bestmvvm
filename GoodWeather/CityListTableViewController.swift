//
//  CityListTableViewController.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import UIKit

class CityListTableViewController: UITableViewController {

    private let vm = CityListTableViewModel()
    private var numberOfItemsDisplayed = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupUI()
        vm.loadWeatherData(for: vm.savedCityNames )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        vm.loadWeatherData(for: vm.savedCityNames, after:3600 )
    }
    
    private func setupUI(){
        if let navBar = self.navigationController?.navigationBar {
            navBar.prefersLargeTitles = true
        }
    }
    
    private func setupBindings(){
        vm.list.bind(listiner: { [unowned self] in
            
            self.numberOfItemsDisplayed = $0.count
            self.tableView.reloadData()
        })
    }
    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfItemsDisplayed
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.weatherCell.rawValue, for: indexPath) as! WeatherCell
        
        let location = vm.cityWeatherViewModelAt(index: indexPath.row)
        
        cell.cityLabel.text = location.cityName
        cell.tempLabel.text = "\(location.temp)°"
        cell.selectionStyle = .none
        
        return cell
    }
}



extension CityListTableViewController: AddLocationViewModelDelegate{

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navVC = segue.destination as? UINavigationController,
           let addLocationVC = navVC.viewControllers.first as? AddLocationViewController {
            
            addLocationVC.viewModelDelegate = self
        }
    }
    
    func viewModelDidChange(newmodel: AddLocationViewModel) {
        vm.loadWeatherData(for: vm.savedCityNames )
    } 
}
