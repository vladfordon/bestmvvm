//
//  Box.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 12/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

class Box<T>{
    
    typealias Listiner = (T) -> Void
    var listiner: Listiner?
    
    var value:T{
        didSet{ listiner?(value) }
    }
    
    init (_ value:T){ self.value = value }
    
    func bind(listiner:Listiner?){
        self.listiner = listiner
        listiner?(value)
    }
}
