//
//  GeneralEnums.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

enum CellIDs:String{
    case weatherCell = "WeatherCell"
}

//If you're using this code, register a free account at: https://openweathermap.org/ to get your own API key, as this might stop working at any moment.

enum ApiKeys:String{
    case openWeatherAPI = "263cf585c819408ce07e82dc3c62c201"
}

enum UserDefaultsKeys:String{
    case locations = "_SavedLocations"
}
