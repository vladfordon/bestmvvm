//
//  WeatherDataModel.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 12/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

struct WeatherDataModel:Decodable{
    
    let mainInfo:BasicWeatherDataModel
    
    private enum CodingKeys:String, CodingKey{
        case mainInfo = "main"
    }
}

struct BasicWeatherDataModel:Decodable{
 
  let temp: String?
  let feelsLike: String?
  let tempMin: String?
  let tempMax: String?
  let pressure: String?
  let humidity: String?
    
  private enum CodingKeys:String, CodingKey{
        case temp, pressure, humidity
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
    
    init(from decoder:Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)

        func setValue(for key:CodingKeys) -> String?{
            if let _value = try? container.decode(Double.self, forKey: key){ return String(_value) }
            else if let _value = try? container.decode(Int.self, forKey: key){ return String(_value) }
            else if let _value = try? container.decode(String.self, forKey: key){ return _value }
            return nil
        }
        
        temp = setValue(for: .temp)
        feelsLike = setValue(for: .feelsLike)
        tempMin = setValue(for: .tempMin)
        tempMax = setValue(for: .tempMax)
        pressure = setValue(for: .pressure)
        humidity = setValue(for: .humidity)
    }
 
}
