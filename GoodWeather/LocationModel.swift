//
//  LocationModel.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 12/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

struct LocationModel{
    
    func save(city:String?){
        
        if let _city = city {
        
            var locations:[String] = (UserDefaults.standard.array(forKey: UserDefaultsKeys.locations.rawValue) as? [String]) ?? [String]()
            
            if !locations.contains(_city){
                
                if locations.count > 12 { locations.remove(at: 0) }
                
                locations.append(_city)
                
                UserDefaults.standard.set(locations, forKey: UserDefaultsKeys.locations.rawValue)
            }
        }
    }
}
