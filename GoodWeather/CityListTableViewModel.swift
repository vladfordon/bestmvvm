//
//  CityListTableViewModel.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

class CityListTableViewModel{
    
    private(set) var list:Box<[CityWeatherViewModel]> = Box([CityWeatherViewModel]())
    private(set) var lastTimeRefreshed = Date()
    
    var savedCityNames:[String]{
        return (UserDefaults.standard.array(forKey: UserDefaultsKeys.locations.rawValue) as? [String]) ?? [String]()
    }
    
    
    func loadWeatherData(for cityNames:[String], webservice:WebService = WebService()){
        
        var currentList = [CityWeatherViewModel]()
        
        for cityName in self.savedCityNames {
            
            guard let url = URLs().openWeatherURL(apiKey: ApiKeys.openWeatherAPI.rawValue, location: cityName) else { continue }
            print(url)
            let resource = Resource<WeatherDataModel>(url: url)
            webservice.load(resource: resource, completion: { [weak self] result in
                
                switch result{
                case .success(let weather):
                    
                    guard let cityWeatherDataViewModel = CityWeatherViewModel(cityName: cityName, weatherDataModel: weather) else { return }
                  
                        currentList.append(cityWeatherDataViewModel)
                        self?.list.value = currentList.sorted(by: { $0.cityName < $1.cityName })
                        self?.lastTimeRefreshed = Date()

                case .failure(let error):
                    print(error)
                    print(cityName)
                }
            })
        }
    }
    
    func loadWeatherData(for cityNames:[String], after:TimeInterval){
   
        let currentInterval = DateInterval(start: lastTimeRefreshed, end: Date())
        let setInterval = DateInterval(start: lastTimeRefreshed, duration: after)
        
        if currentInterval >= setInterval { loadWeatherData(for: cityNames) }
    }
    
    func cityWeatherViewModelAt(index:Int) -> CityWeatherViewModel{ return list.value[index] }
}

struct CityWeatherViewModel{
    
    let cityName: String
    let temp: String
    let feelsLike: String
    let tempMin: String
    let tempMax: String
    let pressure: String
    let humidity: String
    
    init?(cityName:String, weatherDataModel:WeatherDataModel){
        
        if let temp = weatherDataModel.mainInfo.temp,
            let feelsLike = weatherDataModel.mainInfo.feelsLike,
            let tempMin = weatherDataModel.mainInfo.tempMin,
            let tempMax = weatherDataModel.mainInfo.tempMax,
            let pressure = weatherDataModel.mainInfo.pressure,
            let humidity =  weatherDataModel.mainInfo.humidity {
            
           self.cityName = cityName
           self.temp = temp
           self.feelsLike = feelsLike
           self.tempMin = tempMin
           self.tempMax = tempMax
           self.pressure = pressure
           self.humidity = humidity
        }
        else { return nil }
    }
    
    
}
