//
//  URLs.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

struct URLs{
    
    func openWeatherURL(apiKey:String, location:String, units:String = "metric") -> URL? {
        
        let location = location.replacingOccurrences(of: " ", with: "%20")
        
        let stringURL = "https://api.openweathermap.org/data/2.5/weather?q=\(location)&appid=\(apiKey)&units=\(units)"
        guard let url = URL(string: stringURL) else {
            print("URL error: \(stringURL) is not an URL.")
            return nil
        }
        return url
    }
}
