//
//  AddLocationViewController.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import UIKit

class AddLocationViewController: UIViewController {

    @IBOutlet weak var locationTextField:UITextField!
    
    var viewModelDelegate:AddLocationViewModelDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveAction(_ sender: Any){
        if let city = locationTextField.text, city != "" {
           
            let vm = AddLocationViewModel(city: city)
            self.viewModelDelegate.viewModelDidChange(newmodel:vm)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) { self.dismiss(animated: true, completion: nil) }
}
