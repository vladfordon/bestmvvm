//
//  WeatherCell.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation
import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel:UILabel!
    @IBOutlet weak var tempLabel:UILabel!
    
}
