//
//  WebService.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 10/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

struct Resource<T: Decodable>{
    let url:URL
}

enum WebServiceError:Error {
    case decodingError
    case noDataError
}


class WebService{
    
    
    func load<T>(resource:Resource<T>, session:URLSession = URLSession.shared, decoder:JSONDecoder = JSONDecoder(), completion: @escaping (Result<T,WebServiceError>)->() ){
        
        session.dataTask(with: resource.url, completionHandler: { data, response, error in
            
            guard let data = data, error == nil else { print(WebServiceError.noDataError); return }
            
            if let decodedData = try? decoder.decode(T.self, from: data){
                 DispatchQueue.main.async { completion(.success(decodedData))}
            }
            else
            {
                DispatchQueue.main.async { completion(.failure( .decodingError ))}
            }
        }).resume()
    }
}

