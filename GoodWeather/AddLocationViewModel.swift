//
//  AddLocationViewModel.swift
//  GoodWeather
//
//  Created by Wladyslaw Jasinski on 12/02/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

protocol AddLocationViewModelDelegate{
    func viewModelDidChange(newmodel:AddLocationViewModel)
}

struct AddLocationViewModel{
    
    let city:String?
    
    init(city:String?, model:LocationModel = LocationModel() ){
        self.city = city?.capitalized
        model.save(city: self.city)
    }
}
